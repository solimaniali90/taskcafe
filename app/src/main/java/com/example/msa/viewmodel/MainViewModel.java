package com.example.msa.viewmodel;




import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;


import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.msa.service.model.ExploreBody;
import com.example.msa.service.model.Item;
import com.example.msa.service.model.PhotosModel.PhotoItem;
import com.example.msa.service.model.PhotosModel.PhotosBody;
import com.example.msa.service.model.Venue;
import com.example.msa.service.repository.Api;
import com.example.msa.view.ui.Activity.MainActivity;
import com.example.msa.view.ui.Activity.VenuePhotoActivity;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class MainViewModel extends ViewModel implements LocationListener
{
    private MutableLiveData<List<Venue>> albumListLiveData;
    private MutableLiveData<List<PhotoItem>> PhotoListLiveData;

    @Override
    public void onLocationChanged(@NonNull Location location) {
        location.getLongitude();
        location.getLatitude();
        String s= String.valueOf(location.getLatitude());
        String d= String.valueOf(location.getLongitude());

        Log.d(TAG, "Location: " + s+d);
    }
    public LiveData<List<PhotoItem>> getPhoto(){
        PhotoListLiveData=new MutableLiveData<>();

       loadphoto();
        return PhotoListLiveData;

    }

    private void loadphoto()
    {
        String venueId=VenuePhotoActivity.VENUE_ID;

        Call<PhotosBody> call;
        Api apiService = FoursquareClient.getClient().create(Api.class);
        call = apiService.getPictures(venueId, FoursquareClient.CLIENT_ID,
                FoursquareClient.CLIENT_SECRET, Integer.toString(10), FoursquareClient.API_DATE);
        Log.d(TAG, "getPhotosFromFoursquare: " + call.toString());

        call.enqueue(new Callback<PhotosBody>() {

            List<PhotoItem> items;
            @Override
            public void onResponse(Call<PhotosBody> call, Response<PhotosBody> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "API call for pictures was successful. Got " +
                            response.body().getResponse().getPhotos().getCount() + " pictures");
                    items = response.body().getResponse().getPhotos().getItems();

                    PhotoListLiveData.setValue(items);
                    Log.d(TAG, "onResponseee: " + call.request().url().toString());



                }
                else {
                    Log.d(TAG, "API call for pictures was not successful. Error: " + response.errorBody());
                }

            }

            @Override
            public void onFailure(Call<PhotosBody> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());;
            }
        });
    }


    List<Venue> venues;
    //Albums
    public LiveData<List<Venue>> getAlbum()
    {

            albumListLiveData = new MutableLiveData<List<Venue>>();
            loadList();


        return albumListLiveData;
    }

    private void loadList()
    {


        
        String ll= MainActivity.latitude_id+","+MainActivity.longitude_id;

        Log.i("sssss",ll);
        FoursquareClient retrofitBuilder = new FoursquareClient();

        Call<ExploreBody> call = null;
        Api apiService =
                FoursquareClient.getClient().create(Api.class);



        String category=null;
        int limit=50;
        int numPhotos=1;
        String date=FoursquareClient.API_DATE;



        if(category == null) {  //if category is not specified
            call = apiService.getVenues(FoursquareClient.CLIENT_ID,
                    FoursquareClient.CLIENT_SECRET,
                    ll,
                    Integer.toString(limit),
                    Integer.toString(numPhotos),
                    date);
        }
        else {
            call = apiService.getVenuesWithCategory(FoursquareClient.CLIENT_ID,
                    FoursquareClient.CLIENT_SECRET,
                    ll,
                    category,
                    Integer.toString(limit),
                    Integer.toString(numPhotos),
                    date);
        }

        call.enqueue(new Callback<ExploreBody>() {

            @Override
            public void onResponse(Call<ExploreBody> call, Response<ExploreBody> response) {
                if(response.isSuccessful()) {
                    Log.d(TAG, "API call was successful. ");
                    List<Item> items = response.body().getResponse().getGroups().get(0).getItems();
                    venues = Item.ItemsToVenues(items);
                    albumListLiveData.setValue(venues);
                    Log.d(TAG, "onResponseee: " + call.request().url().toString());
                   // progressBar.setVisibility(View.GONE);

                }
                else {
                    Log.d(TAG, "API call was not successful. Error: " + response.errorBody());
                }
              //  showVenuesOnListview();
            }

            @Override
            public void onFailure(Call<ExploreBody> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
             //   showVenuesOnListview();
            }
        });
    }



}
